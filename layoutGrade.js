import { StatusBar } from "expo-status-bar";
import { ScrollView, StyleSheet, Text, View } from "react-native";


export default function LayoutGrade() {
  return (
    <View style={styles.container}>
      <View style={styles.linha}>
        <View style={styles.box1}></View>
        <View style={styles.box2}></View>
      </View>
      <View style={styles.linha}>
        <View style={styles.box3}></View>
        <View style={styles.box4}></View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "center",
    alignItems:'center',
    justifyContent:'center',
  },
  linha:{
    flexDirection:'row'
  },
  box1: {
    width: 50,
    height: 50,
    backgroundColor: "red",
  },
  box2: {
    width: 50,
    height: 50,
    backgroundColor: "green",
  },
  box3: {
    width: 50,
    height: 50,
    backgroundColor: "blue",
  },
  box4: {
    width: 50,
    height: 50,
    backgroundColor: "yellow",
  },
});
