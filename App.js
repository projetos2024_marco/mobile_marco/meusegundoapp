import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import Layout from "./LayutDeTelaEstrutura";
import LayoutHorizontal from './layoutHorizontal'
import LayoutGrade from "./layoutGrade";
import Components from "./components";

export default function App() {
  return (
    <View style={styles.container}>
            <Components></Components>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
  },
  header: {
    height: 50,
    backgroundColor: "blue",
  },

  content: {
    flex: 1,
    backgroundColor: "green",
  },
  footer: {
    height: 50,
    backgroundColor: "red",
  },
});
